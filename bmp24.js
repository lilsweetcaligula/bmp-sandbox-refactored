const Assert = require('assert')
const Lib = require('./lib')

class Bmp24 {
  static async readFrom(readable) {
    Assert(readable, 'readable')

    const bmp_content = await Lib.readInput(readable)
    const bmp_header = Lib.sliceBytes(bmp_content, 0, 2).toString()

    if (bmp_header !== 'BM') {
      throw new NotSupportedBmpFileError(
        "This doesn't look like a BMP file I can understand..."
      )
    }

    return { bmp_content }
  }


  static async writeTo(writable, bmp) {
    Assert.strictEqual(typeof bmp, 'object')

    const bmp_content = Lib.fetchProp(bmp, 'bmp_content')

    return writable.write(bmp_content)
  }


  static imageWidth(bmp) {
    Assert.strictEqual(typeof bmp, 'object')

    const bmp_content = Lib.fetchProp(bmp, 'bmp_content')
    const bytes = Lib.sliceBytes(bmp_content, 0x12, 4)
    const result = bytes.readUInt32LE(0)

    return result
  }


  static imageHeight(bmp) {
    Assert.strictEqual(typeof bmp, 'object')

    const bmp_content = Lib.fetchProp(bmp, 'bmp_content')
    const bytes = Lib.sliceBytes(bmp_content, 0x16, 4)
    const result = bytes.readUInt32LE(0)

    return result
  }


  static pixelArrayOffset(bmp) {
    Assert.strictEqual(typeof bmp, 'object')

    const bmp_content = Lib.fetchProp(bmp, 'bmp_content')
    const bytes = Lib.sliceBytes(bmp_content, 0xa, 4)
    const result = bytes.readUInt32LE(0)

    return result
  }


  static pixelArrayPadded(bmp) {
    Assert.strictEqual(typeof bmp, 'object')

    const array_offset = Bmp24.pixelArrayOffset(bmp)
    const bmp_content = Lib.fetchProp(bmp, 'bmp_content')
    const bytes = Lib.sliceBytes(bmp_content, array_offset)

    return bytes
  }


  static rgbAtOffset(offset, bmp) {
    const array = Bmp24.pixelArrayPadded(bmp)
    const rgb_bytes = Lib.sliceBytes(array, offset, 3)

    const r = rgb_bytes.readUInt8(2)
    const g = rgb_bytes.readUInt8(1)
    const b = rgb_bytes.readUInt8(0)

    return [r, g, b]
  }


  static writeRgbAtOffset(offset, rgb, bmp) {
    Assert.strictEqual(typeof offset, 'number')

    Assert(Array.isArray(rgb))
    Assert.strictEqual(rgb.length, 3)

    const [r, g, b] = rgb

    Assert.strictEqual(typeof r, 'number')
    Assert.strictEqual(typeof g, 'number')
    Assert.strictEqual(typeof b, 'number')

    const array = Bmp24.pixelArrayPadded(bmp)

    array.writeUInt8(r, offset + 2)
    array.writeUInt8(g, offset + 1)
    array.writeUInt8(b, offset + 0)
  }

  static makeRowColToOffsetFunction(width) {
    return (row, col) => 3 * (row * width + col)
  }
}

Bmp24.NotSupportedBmpFileError = class extends Error {}

module.exports = Bmp24
