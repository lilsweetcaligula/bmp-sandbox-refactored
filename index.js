const Assert = require('assert')
const Fs = require('fs')
const Util = require('util')
const Bmp24 = require('./bmp24')
const Rgb = require('./rgb')

; (async () => {
  const bmp = await Bmp24.readFrom(process.stdin)

  // NOTE: Please feel free to write code to transform your
  // 24-bit BMP here.
  //

  await Bmp24.writeTo(process.stdout, bmp)
})()

