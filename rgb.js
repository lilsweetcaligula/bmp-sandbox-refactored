const Assert = require('assert')

class Rgb {
  static equals(rgb1, rgb2) {
    const [r1, g1, b1] = rgb1
    const [r2, g2, b2] = rgb2

    return r1 === r2 &&
      g1 === g2 &&
      b1 === b2
  }

  static red() { return [0xff, 0, 0] }
  static green() { return [0, 0xff, 0] }
  static blue() { return [0, 0, 0xff] }
  static black() { return [0, 0, 0] }

  static isRed(rgb) { return Rgb.equals(rgb, Rgb.red()) }
  static isGreen(rgb) { return Rgb.equals(rgb, Rgb.green()) }
  static isBlue(rgb) { return Rgb.equals(rgb, Rgb.blue()) }
}

module.exports = Rgb
