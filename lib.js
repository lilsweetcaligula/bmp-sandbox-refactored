const Assert = require('assert')
const { AssertionError } = Assert

class Lib {
  static fetchProp(o, name, assertType = x => {}) {
    if (!(name in o)) {
      throw new AssertionError(`The ${name} prop is missing`)
    }

    const x = o[name]

    assertType(x)

    return x
  }

  static sliceBytes(buf, start, n = Infinity) {
    Assert(buf && buf.constructor === Buffer)
    Assert.strictEqual(typeof start, 'number')
    Assert.strictEqual(typeof n, 'number')

    if (n === Infinity) {
      return buf.slice(start)
    }

    return buf.slice(start, start + n)
  }

  static readInput(readable) {
    Assert(readable, 'readable')

    return new Promise((resolve, reject) => {
      let buf
      buf = Buffer.from([])

      readable.once('error', reject)

      readable.on('data', data => {
        buf = Buffer.concat([buf, data])
      })

      readable.once('end', () => {
        resolve(buf)
        return
      })
    })
  }
}

module.exports = Lib
